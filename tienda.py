import sys

art = {}

def anadir(art, precio):
    art[art] = precio

def mostrar():
    print("Lista de artículos en la tienda:")
    for art, precio in art.items():
        print(f"{art}: {precio} euros")

def pedir_art() -> str:
    while True:
        art = input("Artículo: ")
        if art in art:
            return art
        else:
            print("El artículo no está en la lista. Inténtalo de nuevo.")

def pedir_cantidad() -> float:
    while True:
        cantidad_str = input("Cantidad: ")
        try:
            cantidad = float(cantidad_str)
            return cantidad
        except ValueError:
            print("Cantidad no válida. Inténtalo de nuevo.")

def main():
    if len(sys.argv) < 3 or len(sys.argv) % 2 != 1:
        print("Error en argumentos: Debes proporcionar al menos un artículo y su precio.")
        return

    for i in range(1, len(sys.argv), 2):
        art = sys.argv[i]
        precio_str = sys.argv[i + 1]

        try:
            precio = float(precio_str)
            anadir(art, precio)
        except ValueError:
            print(f"Error en argumentos: {precio_str} no es un precio correcto para {art}.")
            return

    mostrar()

    seleccion = pedir_art()
    cantidad = pedir_cantidad()
    precio_unitario = art[seleccion]
    precio_total = cantidad * precio_unitario

    print(f"Compra total: {cantidad} de {seleccion}, a pagar {precio_total} euros.")

if __name__ == '__main__':
    main()
